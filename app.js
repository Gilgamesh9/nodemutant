'use strict'

const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    extended: false
}));

// Habilita o CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

const router = express.Router();

const route = router.get('/', (req, res, next) => {
    res.status(200).send(
        request('https://jsonplaceholder.typicode.com/users',
            function (error, response, body) {
                console.log('error:', error); 
                console.log('statusCode:', response && response.statusCode);
                const parsedUsers = JSON.parse(body);

                const usuariosComSuiteNoEndereco = [];
                let novoIndice = 0;

                function filtrarUsuarios(item, indice) {
                    if (parsedUsers[0].address.suite.indexOf("Suite ") == 0) {
                        usuariosComSuiteNoEndereco[novoIndice] = parsedUsers[indice];
                        novoIndice++;
                    }
                    console.log(item)
                }
                parsedUsers.forEach(filtrarUsuarios)
            })
    );

});

app.use('/', route);

module.exports = app;